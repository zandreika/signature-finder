### Signature finder

#### How to use
`python finder.py search_directory_name signature_for_search`

#### Example
`python finder.py . 68656c6c6f`  
`68656c6c6f` = `hello`

Result will be in `output.txt` file